import sys
sys.path.insert(0, "..")
import torch
import torch.nn as nn
from torchvision import transforms
from cfgs.openimagesv4 import config
from superai.Nets.resnets import resnet50
opt = config()

transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=opt.mean, std=opt.std)
    ])
model = resnet50(30, pretrained=False)
model = model.cuda()
model = torch.nn.DataParallel(model, device_ids=opt.device_ids)


def openimages_infer(images_list, labels_file, weights_path, config_path=None, threshold=0.4):

    with open(labels_file) as f:
        labels = f.readlines()
    f.close()

    save = torch.load(weights_path)
    model.load_state_dict(save["model_weights"])
    model = model.eval()
    mydict = {}
    mydict['predictions'] = []
    for img in images_list:
        normalized_img = transform(img)
        normalized_img.unsqueeze_(0)
        output = model(normalized_img)
        out = torch.sigmoid(output)
        arr = out > threshold
        arr = arr.cpu().numpy()
        arr = arr[0]
        label_ids = [i for i, s in enumerate(arr) if s == 1]
        lab = [labels[int(i)].strip() for i in label_ids]
        probs = [float(out[0][int(i)].cpu().detach().numpy()) for i in label_ids]
        im_dict = {}
        im_dict["classes"] = lab
        im_dict["probs"] = probs
        mydict['predictions'].append(im_dict)

    return [mydict]
